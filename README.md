# Gender-Recognition-by-Voice-Using-Machine-Learning
The project distinguishes between gender of male and female on the basis of voice 
It is a python based project
It uses various machine learning techniques including random forest, decision tree, support vector machine and ada boost.
Dateset used is "voice.csv" from https://www.kaggle.com/primaryobjects/voicegender#voice.csv.
